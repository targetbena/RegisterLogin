//
//  SigninViewController.h
//  LoginScreen
//
//  Created by Mac on 30/1/18.
//  Copyright © 2018 GBS Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SigninViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnSignin;

@end
