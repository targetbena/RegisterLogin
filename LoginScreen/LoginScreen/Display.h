//
//  Display.h
//  LoginScreen
//
//  Created by Mac on 1/2/18.
//  Copyright © 2018 GBS Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface Display : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lbName;

@property (strong, nonatomic) IBOutlet UILabel *lbEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnLogout;
@property (strong, nonatomic) IBOutlet UILabel *lbPassword;



@end
