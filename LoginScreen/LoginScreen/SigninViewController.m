//
//  SigninViewController.m
//  LoginScreen
//
//  Created by Mac on 30/1/18.
//  Copyright © 2018 GBS Technology Co., Ltd. All rights reserved.
//

#import "SigninViewController.h"
#import "Display.h"
#import "ViewController.h"
@interface SigninViewController ()

@end

@implementation SigninViewController

- (IBAction)onBack:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *viewcon = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:viewcon animated:YES completion:nil];
    
}

- (IBAction)OnSignIn:(UIButton *)sender
{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    NSString *name = [user objectForKey:@"Username"];
    long password = [user integerForKey:@"Password"];
    
    NSString *myPassword = [NSString stringWithFormat:@"%li",password];
    
    //check username & password
    
    if ([_txtUsername.text isEqualToString:name] && [_txtPassword.text isEqualToString:myPassword])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        Display *display = [storyboard instantiateViewControllerWithIdentifier:@"Display"];
        [self presentViewController:display animated:YES completion:nil];
        
        NSLog(@"You are Log in ...");
    }
    
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Login Failed"
                                                                       message:@"Username or Password Incorrect."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        NSLog(@"Error ...");
    }

}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _btnSignin.layer.cornerRadius = 15;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
