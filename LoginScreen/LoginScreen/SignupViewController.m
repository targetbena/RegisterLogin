//
//  SignupViewController.m
//  LoginScreen
//
//  Created by Mac on 30/1/18.
//  Copyright © 2018 GBS Technology Co., Ltd. All rights reserved.
//

#import "SignupViewController.h"
#import "ViewController.h"
#import "Display.h"

@interface SignupViewController ()

@end

@implementation SignupViewController

- (IBAction)OnBack:(UIButton *)sender {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *viewCon = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:viewCon animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   _btnSignUp.layer.cornerRadius = 15;
    
}


- (IBAction)OnSignUp:(UIButton *)sender {
    NSString *name = _txtUserName.text;
    NSString *password = _txtPassword.text;
    NSString *email = _txtEmail.text;
    
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    BOOL isemail = [self validateEmailWithString:email];
    NSLog(@"%d",isemail);
   

    NSString *username = [user objectForKey:@"Username"];
    NSString *useremail = [user objectForKey:@"Email"];
    
    // check username & email exist
    if( ![username isEqualToString:name] && ![useremail isEqualToString:email])
    {
        // check username , email & password correction
        if (name.length >= 3 && name.length <=20 && isemail == TRUE && password.length >=3 && password.length<=20)
        {
            [user setObject:name forKey:@"Username"];
            [user setObject:password forKey:@"Password"];
            [user setObject:email forKey:@"Email"] ;
        
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            Display *display = [storyboard instantiateViewControllerWithIdentifier:@"Display"];
            [self presentViewController:display animated:YES completion:nil];
            
        }
    
        else
        {
        
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Sign Up Failed"
                                                                       message:@"Please Input Correct Username ,Email & Password."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        
        }
    }
    else
    {
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Sign Up Failed"
                                                                       message:@"This Username or Email already use"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
        
    
    
}

//check email
- (BOOL)validateEmailWithString :(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


@end
