//
//  ViewController.m
//  LoginScreen
//
//  Created by Mac on 30/1/18.
//  Copyright © 2018 GBS Technology Co., Ltd. All rights reserved.
//

#import "ViewController.h"
#import "SigninViewController.h"
#import "SignupViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _btnSignUp.layer.cornerRadius =15;
    _btnLogin.layer.cornerRadius =15;
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signup:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignupViewController *signVC = [storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
    
    [self presentViewController:signVC animated:YES completion:nil];
    
}

- (IBAction)signin:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SigninViewController *signVC = [storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
    
    [self presentViewController:signVC animated:YES completion:nil];
    
}
@end
