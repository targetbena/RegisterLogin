//
//  Display.m
//  LoginScreen
//
//  Created by Mac on 1/2/18.
//  Copyright © 2018 GBS Technology Co., Ltd. All rights reserved.
//

#import "Display.h"
#import "ViewController.h"
@interface Display()

@end

@implementation Display

- (IBAction)OnLogOut:(UIButton *)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *viewcon = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:viewcon animated:YES completion:nil];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _btnLogout.layer.cornerRadius =15;
    NSUserDefaults *user  = [NSUserDefaults standardUserDefaults];
    _lbName.text = [user objectForKey:@"Username"];
    _lbEmail.text = [user objectForKey:@"Email"];
    _lbPassword.text = [user objectForKey:@"Password"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
